## Tuten prueba timezone

Para compilar y correr el proyecto seguir las siguiente instruccones

1. Clone de bitbucket con su cuenta `git clone https://????????@bitbucket.org/????????/timezone.git`
2. Dentro de la raiz del proyecto escribir:` mvn clean install`
3. Esto genera la carpeta target, dentro de ella escriba el siguiente comando: `java -jar .\timezone-0.0.1-SNAPSHOT.jar`
4. A continuación abra una terminal en la carpeta \timezone\src\main\clientes-app Recuerden que si ejecutan este comando dese sistemas unix deberá estar precedido por sudo, para darle permisos, y si lo ejecutan desde windows recuerden inicial la consola con privilegios de administrador.
`npm install`
5. npm install nos descargara todas las dependencias necesarias para poder ejecutar nuestro proyecto angular en el servidor local.
   Finalmente usamos el comando `ng serve -o` para levantar nuestro servidor.
