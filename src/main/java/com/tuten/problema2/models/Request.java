package com.tuten.problema2.models;

import lombok.Data;

@Data
public class Request {
    private String dato1;
    private int dato2;
}
