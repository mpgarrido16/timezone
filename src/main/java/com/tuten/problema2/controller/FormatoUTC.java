package com.tuten.problema2.controller;

import com.tuten.problema2.models.Request;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = {"*"})
@RestController
@Log4j2
public class FormatoUTC {
    @PostMapping("/formato")
    public ResponseEntity<?> formato(@RequestBody Request request) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date hora = (Date)formatter.parse(request.getDato1());
        calendar.setTime(hora);
        calendar.add(Calendar.HOUR_OF_DAY, request.getDato2());

        String horaResponse = formatter.format(calendar.getTime());

        Map<String, Object> response = new HashMap<>();
        Map<String, Object> content = new HashMap<>();
        content.put("time", horaResponse);
        content.put("timezone", request.getDato2());
        response.put("response", content);

        return new ResponseEntity<Map<String , Object>>(response, HttpStatus.ACCEPTED);
    }
}
