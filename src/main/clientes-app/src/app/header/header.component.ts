import {Component} from '@angular/core';
import Swal from 'sweetalert2';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  title = 'Prueba Tuten';

  constructor(private router: Router) {
  }

}
