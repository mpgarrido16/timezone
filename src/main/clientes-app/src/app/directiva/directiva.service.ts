import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Datos} from "./datos";

@Injectable()
export class DirectivaService {
  private endpoint: string = "http://localhost:8080/formato";

  constructor(private http: HttpClient, private router: Router) {
  }

  getUTC(datos: Datos): Observable<any> {
    return this.http.post<Datos>(this.endpoint, datos);
  }

}
