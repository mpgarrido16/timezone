import {Component, OnInit} from '@angular/core';
import {Datos} from "./datos";
import {DirectivaService} from "./directiva.service";

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html'
})
export class DirectivaComponent {
  title = 'Buscar UTC';
  datos: Datos = new Datos();
  time = "";
  timezone = "";

  constructor(private directivaService: DirectivaService) {
  }

  send(): void {
    console.log(this.datos);
    this.directivaService.getUTC(this.datos)
      .subscribe(response => {
        console.log(response);
        this.time = response.response.time;
        this.timezone = response.response.timezone;
      });
  }

}
